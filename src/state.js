'use strict';

const fs = require('fs');
const deepeq = require('deep-equal');
const deepcp = require('deep-copy');
const wfa = require('write-file-atomic');
const yaml = require('yaml');
const options = require('./options');

const log = s => console.log(`${new Date().toISOString()}: state: ${s}`);

let saved = (() => {
	try {
		const raw = fs.readFileSync(options.state)
			.toString();
		log(`loaded ${raw.length}`);
		return yaml.parse(raw);
	} catch (err) {
		if(err.code !== 'ENOENT') throw err;
		log(`load: ${err.toString()}`);
	}
})() || {};
const db = Object.assign({
	probes: {},
	users: {}
}, JSON.parse(JSON.stringify(saved)));

async function savecore() {
	if(deepeq(db, saved, { strict: true })) return;
	saved = deepcp(db);
	const str = yaml.stringify(db);
	await wfa(options.state, str);
	log(`saved ${str.length}`);
}
let saving;
async function save() {
	saving = saving ? saving.then(savecore) : savecore();
	return await saving;
}

module.exports = { db, save };
