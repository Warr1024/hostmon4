'use strict';

const rethrow = e => { throw e; };
process.on('unhandledRejection', rethrow);

const config = require('./config');
const runprobes = require('./runprobes');
const runusers = require('./runusers');

const log = s => console.log(`${new Date().toISOString()}: ${s}`);

const now = () => new Date()
	.getTime() / 1000;

function cycle(func, name) {
	setTimeout(() => cycle(func, name), Math.random() * 1000 + 500);
	func(config, now(), s => log(`${name}: ${s}`));
}

cycle(runprobes, 'probe');
setTimeout(() => cycle(runusers, 'alert'), 5000);

require('./prometheus');
