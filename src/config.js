'use strict';

const util = require('util');
const fs = require('fs');
const os = require('os');
const yaml = require('yaml');
const deepcp = require('deep-copy');
const options = require('./options');
const underride = require('./underride');
const timing = require('./timing');

const log = s => console.log(`${new Date().toISOString()}: config: ${s}`);

const readFile = util.promisify(fs.readFile);

const isObject = x => !!x && (typeof x === 'object') && x.toString() === '[object Object]';

const fromOptions = (obj, prefix) => Object.keys(options)
	.filter(k => k.startsWith(prefix))
	.map(k => k.substr(prefix.length))
	.filter(k => k && !/-/.test(k))
	.forEach(k => obj[k] = options[prefix + k]);

const probeDefaults = {
	interval: 60,
	jitter: 0.05
};
const userDefaults = {
	squelch: 300,
	heartbeat: 86400,
	preamble: `hostmon4@${os.hostname().replace(/\..*/, '')}`,
	probes: { '*': true }
};
const subDefaults = {
	down: 300,
	repeat: 4 * 3600
};
fromOptions(probeDefaults, 'probe-');
fromOptions(userDefaults, 'user-');
fromOptions(subDefaults, 'sub-');

const empty = { probes: {}, users: {} };

let cachedraw;
const config = deepcp(empty);
let stale;

async function loadconfig() {
	const timer = timing();
	const raw = (await readFile(options.conf))
		.toString();
	log(`read ${raw.length}`);
	if(raw === cachedraw) return log(`cached`);
	cachedraw = raw;

	try {
		const data = yaml.parse(raw);
		underride(data, deepcp(empty));
		Object.entries(data.probes)
			.forEach(([k, v]) => {
				underride(v, { type: 'ping' });
				const pt = require(`./probes/${v.type}`);
				fromOptions(pt.defaults, `probe-${v.type}-`);
				underride(v, pt.defaults, probeDefaults, { name: k, host: k });
			});
		Object.entries(data.users)
			.forEach(([k, v]) => {
				underride(v, userDefaults, { email: k });
				if(v.probes['*'])
					for(let k of Object.keys(data.probes))
						if(!v.probes.hasOwnProperty(k))
							v.probes[k] = v.probes['*'];
				delete v.probes['*'];
				for(let k of Object.keys(v.probes)) {
					if(!data.probes[k]) continue;
					if(!isObject(v.probes[k]))
						v.probes[k] = {};
					const defs = Object.assign({}, subDefaults);
					Object.keys(defs)
						.filter(x => data.probes[k][x])
						.forEach(x => defs[x] = data.probes[k][x]);
					Object.keys(defs)
						.filter(x => v[x])
						.forEach(x => defs[x] = v[x]);
					underride(v.probes[k], defs);
				}
			});

		Object.keys(config)
			.forEach(k => delete config[k]);
		Object.assign(config, data);
		stale = undefined;

		log(`loaded ${timer()} ${yaml.stringify(config).length}`);
	} catch (err) {
		const msg = err.toString();
		log(`failed ${timer()}: ${msg}`);
		stale = { timer, msg };
	}
}

setInterval(() => {
	if(!stale) return;
	log(`!!! CONFIG NOT LOADED ${stale.timer()}: ${stale.msg}`);
}, 10000);

let pending = loadconfig();
fs.watchFile(options.conf, { interval: 2000 },
	() => pending = pending.then(loadconfig));

module.exports = config;
