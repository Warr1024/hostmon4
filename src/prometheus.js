'use strict';

const http = require('http');
const options = require('./options');
const state = require('./state');
const config = require('./config');

if(options.prometheus) {
	let host = '0.0.0.0';
	let port = 30080;
	if(options.prometheus !== true) {
		const parts = `${options.prometheus}`.split(':');
		if(`${parts[0]}` === `${Number(parts[0])}`)
			port = Number(parts[0]);
		else {
			host = parts[0] || host;
			port = Number(parts[1]) || port;
		}
	}

	const now = () => new Date()
		.getTime() / 1000;
	const gauge = (res, objs, name, help, labels, value) => {
		if(!Array.isArray(objs)) objs = Object.entries(objs)
			.map(([k, v]) => ({ k, v }));
		res.write(`# HELP ${name} ${help}\n# TYPE ${name} gauge\n`);
		objs.forEach(o => res.write(`${name}{${Object.entries(labels(o))
			.map(([k, v]) => `${k}=${JSON.stringify(v)}`).join(',')
				}} ${value(o)}\n`));
		res.write('\n');
	};
	const server = http.createServer((req, res) => {
		if(req.url.endsWith('favicon.ico')) {
			res.statusCode = 404;
			return res.end();
		}

		res.setHeader('Content-Type', 'text/plain');

		gauge(res, state.db.probes,
			'state_probe_last', 'Seconds since last attempt',
			x => ({ probe: x.k }), x => now() - x.v.last);
		gauge(res, state.db.probes,
			'state_probe_next', 'Seconds until next attempt',
			x => ({ probe: x.k }), x => x.v.next - now());
		gauge(res, state.db.probes,
			'state_probe_success', 'Seconds since last success',
			x => ({ probe: x.k }), x => now() - x.v.success);

		gauge(res, state.db.users,
			'state_user_alert', 'Seconds since last alert sent',
			x => ({ user: x.k, sent: x.v.sent }),
			x => now() - x.v.when);

		gauge(res, config.probes,
			'config_probe_interval', 'Seconds between probe runs',
			x => ({ probe: x.k, type: x.v.type }),
			x => x.v.interval);
		gauge(res, config.probes,
			'config_probe_jitter', 'Probe timing jitter ratio',
			x => ({ probe: x.k, type: x.v.type }),
			x => x.v.jitter);

		gauge(res, config.users,
			'config_user_squelch', 'Minimum seconds between alerts',
			x => ({ user: x.k }), x => x.v.squelch);
		gauge(res, config.users,
			'config_user_heartbeat', 'Maximum seconds between alerts',
			x => ({ user: x.k }), x => x.v.heartbeat);

		const subs = Object.entries(config.users)
			.flatMap(([uname, user]) =>
				Object.entries(user.probes)
				.map(([pname, v]) => Object.assign({}, v, { uname, pname })));
		gauge(res, subs, 'config_sub_down', 'Minimum seconds to consider probe down',
			x => ({ user: x.uname, probe: x.pname }), x => x.down);
		gauge(res, subs, 'config_sub_repeat', 'Maximum seconds to repeat alert',
			x => ({ user: x.uname, probe: x.pname }), x => x.repeat);

		res.end();
	});
	server.listen(port, host, () =>
		console.log(`prometheus metrics ready at http://${host}:${port}`));
}
