'use strict';

const ntp = require('ntp-client');

module.exports.defaults = {
	interval: 300,
	down: 900,
	host: 'pool.ntp.org',
	port: 123,
	timeout: 5,
	maxdrift: 60
};

module.exports.run = async (config, state, log) => {
	const date = await new Promise((res, rej) => {
		const abort = setTimeout(() => rej('timeout'), config.timeout * 1000);
		ntp.getNetworkTime(config.host, config.port, (err, date) => {
			clearTimeout(abort);
			return err ? rej(err) : res(date);
		});
	});
	const drift = Math.abs(date.getTime() - new Date()
		.getTime()) / 1000;
	log(`drift: ${drift}`);
	if(drift > config.maxdrift)
		throw `drift ${drift} > max ${config.maxdrift}`;
};
