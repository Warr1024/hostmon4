'use strict';

const crypto = require('crypto');
const https = require('https');
const needle = require('needle');
const spa = require('socks-proxy-agent');

module.exports.defaults = {
	useragent: 'hostmon/4',
	timeout: 30,
	allow200: true,
	allow204: true,
	allow301: true,
	allow302: true,
	torhost: '127.0.0.1',
	torport: 9050
};

module.exports.run = async (config, state, log) => {
	const url = new URL(config.url);
	const opts = {};
	if(config.tor || url.hostname.endsWith('.onion'))
		opts.agent = new spa({
			host: config.torhost,
			port: config.torport,
		});
	else if(url.protocol === 'https:')
		opts.agent = new https.Agent({
			// https://github.com/nodejs/node/issues/3940#issuecomment-307123618
			maxCachedSessions: 0
		});
	const resp = await new Promise((res, rej) => {
		const abort = setTimeout(() => rej('timeout'), config.timeout * 1000);
		needle('get', config.url, undefined, opts)
			.then(res)
			.catch(rej)
			.then(() => clearTimeout(abort));
	});
	log(`response ${resp && resp.statusCode}`);
	if(config.tlsdays) {
		const expire = new Date(resp.socket.getPeerCertificate()
				.valid_to)
			.getTime() / 1000;
		const now = new Date()
			.getTime() / 1000;
		const days = (expire - now) / 86400;
		log(`tlsdays: ${days}`);
		if(!(days >= config.tlsdays)) // ! > guards against NaN
			throw `cert expires in ${days} days`;
	}
	if(config.tlskey) {
		const fp = crypto.createHash('sha256')
			.update(resp.socket.getPeerCertificate()
				.pubkey)
			.digest('base64')
			.replace(/=+$/, '');
		log(`tlskey: ${fp}`);
		const expect = `${config.tlskey}`.replace(/=+$/, '');
		if(fp !== expect)
			throw `tls key mismatch ${fp} != ${expect}`;
	}
	if(!resp || !resp.statusCode || !config[`allow${resp.statusCode}`])
		throw `bad statusCode ${resp && resp.statusCode}`;
};
