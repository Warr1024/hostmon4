'use strict';

const mysys = require('../../mysys');

module.exports.defaults = {
	timeout: 10,
	'param-c': 1,
	'param-i': 10,
	'param-s': 1
};

module.exports.run = async (config, state, log) => {
	const opts = [];
	Object.keys(config)
		.filter(k => k.startsWith('param'))
		.forEach(k => opts.push(k.substr('param'.length), config[k]));
	opts.push(config.host)
	await mysys('ping', opts, log, config.timeout);
};
