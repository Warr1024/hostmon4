'use strict';

const tls = require('tls');

module.exports.defaults = {
	interval: 300,
	down: 900,
	port: 443,
	useragent: 'hostmon/4',
	timeout: 30,
	mindays: 21
};

module.exports.run = async (config, state, log) => {
	const sock = tls.connect({
		host: config.host,
		port: config.port,
	});
	await new Promise((res, rej) => {
		const abort = setTimeout(rej, config.timeout * 1000);
		sock.on('secureConnect', () => {
			clearTimeout(abort);
			res();
		});
	});
	const expire = new Date(sock.getPeerCertificate()
			.valid_to)
		.getTime() / 1000;
	const now = new Date()
		.getTime() / 1000;
	const days = (expire - now) / 86400;
	log(`days: ${days}`);
	if(!(days >= config.mindays)) // ! > guards against NaN
		throw `cert expires in ${days} days`;
};
