'use strict';

const net = require('net');
const socks = require('socks');
const mysys = require('../../mysys');

module.exports.defaults = {
	timeout: 30,
	port: 22,
	keytype: 'ed25519',
	torhost: '127.0.0.1',
	torport: 9050,
};

module.exports.run = async (config, state, log) => {
	const ctx = Object.assign({}, config);

	if(ctx.tor || ctx.host.endsWith('.onion')) {
		const port = Math.floor(49152 + Math.random() * 16384);
		ctx.svr = net.createServer();
		ctx.svr.listen(port, '127.0.0.1');
		ctx.socks = await socks.SocksClient.createConnection({
			proxy: { host: ctx.torhost, port: ctx.torport, type: 5 },
			command: 'connect',
			destination: { host: ctx.host, port: ctx.port }
		});
		ctx.svr.on('connection', sock => {
			ctx.socks.socket.pipe(sock);
			sock.pipe(ctx.socks.socket);
		});
		log(`127.0.0.1:${port} -> ${ctx.host}:${ctx.port}`);
		ctx.host = '127.0.0.1';
		ctx.port = port;
	}

	let buff = '';
	await mysys('ssh-keyscan', ['-t', ctx.keytype, '-D', '-p', ctx.port, ctx.host],
		log, config.timeout, proc => proc.stdout.on('data', x => buff += x.toString()));
	if(!buff.includes(ctx.expect))
		throw `expected ${ctx.keytype} key FP ${ctx.expect} absent`;

	if(ctx.svr) ctx.svr.close();
	if(ctx.socks) ctx.socks.socket.destroy();
};
