'use strict';

const crypto = require('crypto');
const needle = require('needle');

module.exports.defaults = {
	interval: 300,
	down: 900,
	url: 'https://google.com/',
	timeout: 5,
	maxdrift: 60
};

module.exports.run = async (config, state, log) => {
	const date = await new Promise((res, rej) => {
		const abort = setTimeout(() => rej('timeout'), config.timeout * 1000);
		needle('get', `${config.url}${config.url.match(/\?/)?'&':'?'}_=${
			crypto.randomBytes(16).toString('hex')}`)
			.then(resp => {
				try {
					res(new Date(resp.headers.date));
				} catch (err) {
					rej(err);
				}
			})
			.catch(rej)
			.then(() => clearTimeout(abort));
	});
	const drift = Math.abs(date.getTime() - new Date()
		.getTime()) / 1000;
	log(`drift: ${drift}`);
	if(drift > config.maxdrift)
		throw `drift ${drift} > max ${config.maxdrift}`;
};
