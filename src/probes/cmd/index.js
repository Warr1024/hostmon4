'use strict';

const mysys = require('../../mysys');

module.exports.defaults = {
	shell: '/bin/sh',
	shellargs: ['-c'],
	cmd: 'false',
	timeout: 30
};

module.exports.run = async (config, state, log) => {
	await mysys(config.shell, [...config.shellargs, config.cmd], log, config.timeout);
};
