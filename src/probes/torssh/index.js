'use strict';

const net = require('net');

module.exports.defaults = {
	down: 1800,
	useragent: 'hostmon/4',
	torhost: '127.0.0.1',
	torport: 9050,
	sshport: 22,
	expect: 'SSH-'
};

const buffdump = buff => JSON.stringify(buff.toString())
	.replace(/\\u00/g, '\\')
	.replace(/^"/, '')
	.replace(/"$/, '');

module.exports.run = async (config, state, log) => {
	const namelen = Buffer.alloc(1);
	namelen.writeInt8(config.host.length);
	const portbuff = Buffer.alloc(2);
	portbuff.writeInt16BE(config.sshport);
	const sendbuff = Buffer.concat([
		Buffer.from('05010005010003', 'hex'),
		namelen,
		Buffer.from(config.host, 'ascii'),
		portbuff,
		Buffer.from(config.useragent + '\n', 'ascii')
	]);

	const expect = Buffer.concat([
		Buffer.from('050005000001000000000000', 'hex'),
		Buffer.from(config.expect, 'ascii')
	]);

	const sock = net.createConnection({ port: config.torport, host: config.torhost });
	sock.write(sendbuff);
	await new Promise((res, rej) => {
		sock.on('error', rej);
		sock.on('close', rej);
		let recvbuff = Buffer.alloc(0);
		sock.on('data', x => {
			recvbuff = Buffer.concat([recvbuff, x]);
			if(recvbuff.length >= expect.length) {
				if(recvbuff.slice(0, expect.length)
					.toString('hex') !==
					expect.toString('hex'))
					return rej(
						`expect failed ${buffdump(recvbuff)
						} != ${buffdump(expect)}`
					);
				log(buffdump(recvbuff));
				res();
				sock.destroy();
			}
		});
	});
};
