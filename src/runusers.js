'use strict';

const moment = require('moment-timezone');
const state = require('./state');
const underride = require('./underride');
const mysys = require('./mysys');
const timing = require('./timing');
const options = require('./options');
const { probing } = require('./runprobes');

function dtfmt(stamp, now, user) {
	if (stamp <= 0)
		return 'never';
	const tz = moment(new Date(stamp * 1000))
		.tz(user.tz);
	const ago = now - stamp;
	if (ago < 86400)
		return tz.format('HH:mm');
	if (ago < 86400 * 7)
		return tz.format('ddd HH:mm')
			.toLowerCase();
	if (ago < 86400 * 365)
		return tz.format('MM-DD HH:mm');
	return tz.format('YYYY-MM-DD HH:mm');
}

const alerting = {};

function runUsers(rc, now, log) {
	for (let v of Object.values(rc.users)) {
		if (alerting[v.email] || v.disable) continue;

		const st = state.db.users[v.email] = state.db.users[v.email] || {};
		underride(st, { sent: '', when: 0 });
		if (v.squelch && (now < st.when + v.squelch)) continue;

		const order = {};
		const downdb = {};
		for (let [n, o] of Object.entries(v.probes)) {
			if (!rc.probes[n] || rc.probes[n].disable) continue;
			const pst = state.db.probes[n] || {};
			const downtime = now - o.down;
			const pn = probing[n];
			const isdown = pn && (pn.started < downtime) ||
				((pst.last < downtime) ||
				((pst.last - pst.success) > o.down));
			if (!isdown) continue;
			const desc = dtfmt(pst.success, now, v);
			order[desc] = pst.success;
			downdb[desc] = downdb[desc] || {};
			downdb[desc][o.alias || n] = true;
		}
		const rpt = !Object.keys(downdb)
			.length ? 'CLEAR' :
			`DOWN: ${Object.keys(downdb).sort((a, b) => order[a] - order[b])
				.map(d => `[${d}] ${Object.keys(downdb[d]).sort().join(' ')}`)
				.join(' ')}`;
		if (rpt === st.sent && (!v.heartbeat || now < st.when + v.heartbeat)) continue;

		const mylog = s => log(`${v.email}: ${s}`);
		mylog(`alerting: ${rpt}`);
		const timer = timing();
		const args = [...(options.mta.split(' ')), v.email];
		const cmd = args.shift();
		alerting[v.email] = mysys(cmd, args, mylog, 300, proc => proc.stdin.end(
			`To: ${v.email}${v.subject ? `\nSubject: ${v.subject}` : ''}\nDate: ${moment().tz('UTC').format('ddd, DD MMM YYYY HH:mm:ss ZZ')
			}\nContent-Type: text/plain; charset=utf-8\n\n${v.preamble ?
				`${v.preamble} ` : ''}${rpt}`))
			.then(() => {
				st.when = now;
				st.sent = rpt;
				state.save();
				mylog(`success ${timer()}`);
			})
			.catch(e => mylog(`failed ${timer()}: ${e.toString()}`))
			.then(() => delete alerting[v.email]);
	}
}

module.exports = runUsers;
