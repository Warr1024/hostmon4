'use strict';

const now = () => new Date()
	.getTime() / 1000;

const timing = () => {
	const started = now();
	return () => Math.round((now() - started) * 1000) / 1000;
};

module.exports = timing;
