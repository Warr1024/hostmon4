'use strict';

module.exports = require('minimist')(process.argv.slice(2), {
	default: {
		conf: 'config.yaml',
		state: 'state.yaml',
		mta: 'sendmail'
	}
});
