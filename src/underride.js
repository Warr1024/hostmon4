'use strict';

module.exports = (a, ...b) => b.forEach(o => Object.keys(o)
	.filter(k => !a.hasOwnProperty(k))
	.forEach(k => a[k] = o[k]));
