'use strict';

const state = require('./state');
const underride = require('./underride');
const timing = require('./timing');

const boxmuller = () => Math.sqrt(-2 * Math.log(Math.random())) *
	Math.sin(2 * Math.PI * Math.random());

const probing = {};

function runprobes(rc, now, log) {
	for (let v of Object.values(rc.probes)) {
		if (probing[v.name] || v.disable) continue;
		const st = state.db.probes[v.name] = state.db.probes[v.name] || {};
		underride(st, { last: 0, next: 0, success: 0 });
		if (now < st.next) continue;
		const mylog = s => log(`${v.name}: ${s}`);
		mylog('starting');
		const timer = timing();
		probing[v.name] = {
			started: now,
			running: (async () => await require(`./probes/${v.type}`)
				.run(v, state.db.probes[v.name], mylog))()
				.then(() => st.success = now)
				.catch(e => mylog(e.toString()))
				.then(() => {
					st.last = now;
					st.next = now + v.interval * (1 + v.jitter * boxmuller());
					state.save();
					mylog(`ran ${timer()} down ${st.last - st.success}`);
					delete probing[v.name];
				})
		};
	}
}
runprobes.probing = probing;

module.exports = runprobes;
