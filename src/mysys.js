'use strict';

const childproc = require('child_process');
const readline = require('readline');

function pipeout(stream, log) {
	readline.createInterface({ input: stream })
		.on('line', l => {
			if(/\S/.test(l))
				log(`> ${l}`);
		});
}

async function mysys(cmd, args, log, timeout, cb) {
	log(`# ${cmd} ${args.join(' ')}`);
	const proc = childproc.spawn(cmd, args, { stdio: 'pipe pipe pipe'.split(' ') });
	const abort = setTimeout(() => {
		try { proc.kill(9); } catch (e) {}
	}, timeout * 1000);
	try {
		return await new Promise((res, rej) => {
			proc.on('error', rej);
			proc.stdout.on('error', rej);
			proc.stderr.on('error', rej);
			const outclose = new Promise(r => proc.stdout.on('close', r));
			const errclose = new Promise(r => proc.stderr.on('close', r));
			const bothclose = Promise.all([errclose, outclose]);
			proc.on('close', (code, signal) => bothclose.then(() =>
				signal ? rej(`signal ${signal}`) :
				code ? rej(`code ${code}`) : res()));
			pipeout(proc.stdout, log);
			pipeout(proc.stderr, log);
			if(cb) cb(proc);
		});
	} finally { clearTimeout(abort); }
}

module.exports = mysys;
