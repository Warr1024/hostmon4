TAG=warr1024/hostmon4:latest

build:
	docker build -t ${TAG} .

push: build
	docker push ${TAG}

run: build
	docker run -i --rm \
		-v `pwd`/data:/app/data \
		-p 30080 \
		${TAG} \
		--conf=/app/data/config.yaml \
		--state=/app/data/state.yaml \
		--prometheus

setup:
	find . -name node_modules -prune -or -type d -exec [ -f {}/package.json -a ! -d {}/node_modules ] \; -print0 |\
	xargs -0rtn 1 -P 8 -- sh -c 'cd "$$1" && mkdir -p node_modules && exec npm ci -dddd' --

test: setup
	node src --conf=`pwd`/data/config.yaml --state=`pwd`/data/state.yaml --prometheus

clean:
	find . -type d -name node_modules -print0 | xargs -0rt rm -rf
