FROM alpine:3.15.4

RUN adduser -h /app -D app
WORKDIR /app

RUN apk add --no-cache \
	npm \
	make \
	supervisor \
	tor \
	exim \
	openssh-client

RUN chmod u+s /bin/ping

ENTRYPOINT ["/bin/sh", "/app/entry.sh"]

USER app

COPY --chown=app:app Makefile src/ /app/
RUN make setup && rm -rf /app/.npm

COPY --chown=app:app entry.sh start.sh supervisord.conf /app/
